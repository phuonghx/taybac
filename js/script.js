$("#menu-toggle").click(function(e) {

    e.preventDefault();

    $("#wrapper").toggleClass("toggled");
});

$("#menu-toggle").click(function(e) {
 	e.preventDefault();
 	$("#left-right").toggleClass('fa-caret-left');
 	$("#left-right").toggleClass('fa-caret-right');
});

/* Full screen */
$(document).ready(function () {
    //Toggle fullscreen
    $("#panel-fullscreen").click(function (e) {
        e.preventDefault();
        
        var $this = $(this);
    
        if ($this.children('i').hasClass('fa-arrows-alt'))
        {
            $this.children('i').removeClass('fa-arrows-alt');
            $this.children('i').addClass('fa-compress');
        }
        else if ($this.children('i').hasClass('fa-compress'))
        {
            $this.children('i').removeClass('fa-compress');
            $this.children('i').addClass('fa-arrows-alt');
        }
        $('#panel-body').toggleClass('full-height');
        $(this).closest('.panel').toggleClass('panel-fullscreen');
    });
});

/* Set width combosearch */
$(document).ready(function () {
    var searchInput = $('#search-input').outerWidth();
    var searchSubmit = $('#search-submit').outerWidth();
    var searchForm = $('#search-form').outerWidth();
    
    // $('#combo-search-button').width(searchSubmit + searchInput);
});
